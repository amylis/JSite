/**
 * Copyright &copy; 2017-2019 <a href="https://gitee.com/baseweb/JSite">JSite</a> All rights reserved.
 */
package com.jsite.modules.flowable.dao;

import com.jsite.common.persistence.CrudDao;
import com.jsite.common.persistence.annotation.MyBatisDao;
import com.jsite.modules.flowable.entity.FlowForm;
import com.jsite.modules.flowable.entity.FormUser;
import com.jsite.modules.flowable.entity.FormVersion;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 流程表单生成DAO接口
 * @author liuruijun
 * @version 2019-04-21
 */
@MyBatisDao
public interface FlowFormDao extends CrudDao<FlowForm> {
	FlowForm getEntity(FlowForm flowForm);

	/**
	 * 维护表单与人员权限关系
	 * @param flowForm
	 * @return
	 */
	int deleteFormUser(FlowForm flowForm);

	int insertFormUser(FlowForm flowForm);

	List<FormUser> findUserForm(@Param("userId") String userId);


    /**
     * 维护表流程实例绑定的表单版本信息
     * @param formVersion
     * @return
     */
    int insertFormVersion(FormVersion formVersion);
    int deleteFormVersion(FormVersion formVersion);
    FormVersion findFormVersion(@Param("procInsId") String procInsId);
}